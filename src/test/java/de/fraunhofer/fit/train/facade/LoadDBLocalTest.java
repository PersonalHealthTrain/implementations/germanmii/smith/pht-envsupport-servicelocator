package de.fraunhofer.fit.train.facade;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import de.fraunhofer.fit.train.config.AppConfig;
import de.fraunhofer.fit.train.model.EnvENUM;
import de.fraunhofer.fit.train.model.EnvironmentProperties;
import de.fraunhofer.fit.train.model.dev.DEVenvironmentProperties;
import de.fraunhofer.fit.train.model.dev.MicroservicesPropertiesDEVModel;
import de.fraunhofer.fit.train.model.dev.MongoMicroservicesPropertiesDEVModel;
import de.fraunhofer.fit.train.model.dev.TrainModellingToolPropertiesDEVModel;
import de.fraunhofer.fit.train.model.dev.TrainNodesPropertiesDEVModel;
import de.fraunhofer.fit.train.model.dev.URIMicroservicesPropertiesDEVModel;
import de.fraunhofer.fit.train.model.dev.WebdavMicroServicesPropertiesDEVModel;
import de.fraunhofer.fit.train.model.prod.MicroservicesPropertiesPRODModel;
import de.fraunhofer.fit.train.model.prod.MongoMicroservicesPropertiesPRODModel;
import de.fraunhofer.fit.train.model.prod.PRODenvironmentProperties;
import de.fraunhofer.fit.train.model.prod.TrainModellingToolPropertiesPRODModel;
import de.fraunhofer.fit.train.model.prod.TrainNodesPropertiesPRODModel;
import de.fraunhofer.fit.train.model.prod.URIMicroservicesPropertiesPRODModel;
import de.fraunhofer.fit.train.model.prod.WebdavMicroServicesPropertiesPRODModel;
import de.fraunhofer.fit.train.model.test.MicroservicesPropertiesTESTModel;
import de.fraunhofer.fit.train.model.test.MongoMicroservicesPropertiesTESTModel;
import de.fraunhofer.fit.train.model.test.TESTenvironmentProperties;
import de.fraunhofer.fit.train.model.test.TrainModellingToolPropertiesTESTModel;
import de.fraunhofer.fit.train.model.test.TrainNodesPropertiesTESTModel;
import de.fraunhofer.fit.train.model.test.URIMicroservicesPropertiesTESTModel;
import de.fraunhofer.fit.train.model.test.WebdavDocumentationPropertiesTESTModel;
import de.fraunhofer.fit.train.model.test.WebdavMetadataPropertiesTESTModel;
import de.fraunhofer.fit.train.model.test.WebdavMicroServicesPropertiesTESTModel;
import de.fraunhofer.fit.train.model.test.WebdavPagePropertiesTESTModel;
import de.fraunhofer.fit.train.persistence.IEnvironmentPropertiesRepository;



@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {AppConfig.class})
@ComponentScan({"de.fraunhofer.fit.train"})
@EntityScan("de.fraunhofer.fit.train")
@SpringBootApplication
public class LoadDBLocalTest {
	
	
	@Autowired IEnvironmentPropertiesRepository repository;
	

	
	@Test
	public void trainMockNotNull() {
		Assert.assertNotNull(repository);
	}
	
	
	@Test
	public void targetInsert() throws NoSuchAlgorithmException, IOException {
		repository.deleteAll();
		repository.save(getEnvMockProperties());
	}
	
	public EnvironmentProperties getEnvMockProperties() {
		
		EnvironmentProperties evprops = new EnvironmentProperties();
		DEVenvironmentProperties dev = new DEVenvironmentProperties();
		dev.setEnv(EnvENUM.DEV.toString());
		MicroservicesPropertiesDEVModel microservicesPropertiesDEVModel = new MicroservicesPropertiesDEVModel();
		microservicesPropertiesDEVModel.setHost("0.0.0.0");
		microservicesPropertiesDEVModel.setPort("9091");
		microservicesPropertiesDEVModel.setUser("admin");
		microservicesPropertiesDEVModel.setPass("21232f297a57a5a743894a0e4a801fc3");
		dev.setMicroservicesPropertiesDEVModel(microservicesPropertiesDEVModel);
				
		MongoMicroservicesPropertiesDEVModel mongoMicroservicesPropertiesDEVModel = new MongoMicroservicesPropertiesDEVModel();
		mongoMicroservicesPropertiesDEVModel.setHost("cloud41.dbis.rwth-aachen.de");
		mongoMicroservicesPropertiesDEVModel.setPort("27017");
		mongoMicroservicesPropertiesDEVModel.setUser("admin");
		mongoMicroservicesPropertiesDEVModel.setPass("21232f297a57a5a743894a0e4a801fc3");
		dev.setMongoMicroservicesPropertiesDEVModel(mongoMicroservicesPropertiesDEVModel);
	
		TrainModellingToolPropertiesDEVModel trainModellingToolPropertiesDEVModel = new TrainModellingToolPropertiesDEVModel();
		trainModellingToolPropertiesDEVModel.setHost("127.0.0.1");
		trainModellingToolPropertiesDEVModel.setPort("1880");
		trainModellingToolPropertiesDEVModel.setUser("admin");
		trainModellingToolPropertiesDEVModel.setPass("21232f297a57a5a743894a0e4a801fc3");
		dev.setTrainModellingToolPropertiesDEVModel(trainModellingToolPropertiesDEVModel);
		
		TrainNodesPropertiesDEVModel trainNodesPropertiesDEVModel = new TrainNodesPropertiesDEVModel();
		trainNodesPropertiesDEVModel.setHost("127.0.0.1");
		trainNodesPropertiesDEVModel.setPort("1880");
		trainNodesPropertiesDEVModel.setUser("admin");
		trainNodesPropertiesDEVModel.setPass("21232f297a57a5a743894a0e4a801fc3");
		dev.setTrainNodesPropertiesDEVModel(trainNodesPropertiesDEVModel);
		
		WebdavMicroServicesPropertiesDEVModel webdavMicroServicesPropertiesDEVModel = new WebdavMicroServicesPropertiesDEVModel();
		webdavMicroServicesPropertiesDEVModel.setHost("127.0.0.1");
		webdavMicroServicesPropertiesDEVModel.setPort("9999");
		webdavMicroServicesPropertiesDEVModel.setUser("admin");
		webdavMicroServicesPropertiesDEVModel.setPass("21232f297a57a5a743894a0e4a801fc3");
		dev.setWebdavMicroServicesPropertiesDEVModel(webdavMicroServicesPropertiesDEVModel);
		
		
		URIMicroservicesPropertiesDEVModel uriMicroServicesPropertiesDEVModel = new URIMicroservicesPropertiesDEVModel();
		uriMicroServicesPropertiesDEVModel.setHost("127.0.0.1");
		uriMicroServicesPropertiesDEVModel.setPort("8888");
		uriMicroServicesPropertiesDEVModel.setUser("admin");
		uriMicroServicesPropertiesDEVModel.setPass("21232f297a57a5a743894a0e4a801fc3");
		dev.setUriMicroservicesPropertiesDEVModel(uriMicroServicesPropertiesDEVModel);
		
		evprops.setDEVenvironmentProperties(dev);
		
		//==PROD
		PRODenvironmentProperties prod = new PRODenvironmentProperties();
		prod.setEnv("PROD");
		MicroservicesPropertiesPRODModel microservicesPropertiesPRODModel = new MicroservicesPropertiesPRODModel();
		microservicesPropertiesPRODModel.setHost("127.0.0.1");
		microservicesPropertiesPRODModel.setPort("9091");
		microservicesPropertiesPRODModel.setUser("admin");
		microservicesPropertiesPRODModel.setPass("21232f297a57a5a743894a0e4a801fc3");
		prod.setMicroservicesPropertiesPRODModel(microservicesPropertiesPRODModel);
				
		MongoMicroservicesPropertiesPRODModel mongoMicroservicesPropertiesPRODModel = new MongoMicroservicesPropertiesPRODModel();
		mongoMicroservicesPropertiesPRODModel.setHost("127.0.0.1");
		mongoMicroservicesPropertiesPRODModel.setPort("27017");
		mongoMicroservicesPropertiesPRODModel.setUser("admin");
		mongoMicroservicesPropertiesPRODModel.setPass("21232f297a57a5a743894a0e4a801fc3");
		prod.setMongoMicroservicesPropertiesPRODModel(mongoMicroservicesPropertiesPRODModel);
	
		TrainModellingToolPropertiesPRODModel trainModellingToolPropertiesPRODModel = new TrainModellingToolPropertiesPRODModel();
		trainModellingToolPropertiesPRODModel.setHost("127.0.0.1");
		trainModellingToolPropertiesPRODModel.setPort("1880");
		trainModellingToolPropertiesPRODModel.setUser("admin");
		trainModellingToolPropertiesPRODModel.setPass("21232f297a57a5a743894a0e4a801fc3");
		prod.setTrainModellingToolPropertiesPRODModel(trainModellingToolPropertiesPRODModel);
		
		
		TrainNodesPropertiesPRODModel trainNodesPropertiesPRODModel = new TrainNodesPropertiesPRODModel();
		trainNodesPropertiesPRODModel.setHost("127.0.0.1");
		trainNodesPropertiesPRODModel.setPort("1880");
		trainNodesPropertiesPRODModel.setUser("admin");
		trainNodesPropertiesPRODModel.setPass("21232f297a57a5a743894a0e4a801fc3");
		prod.setTrainNodesPropertiesPRODModel(trainNodesPropertiesPRODModel);
		
		WebdavMicroServicesPropertiesPRODModel webdavMicroServicesPropertiesPRODModel = new WebdavMicroServicesPropertiesPRODModel();
		webdavMicroServicesPropertiesPRODModel.setHost("127.0.0.1");
		webdavMicroServicesPropertiesPRODModel.setPort("9999");
		webdavMicroServicesPropertiesPRODModel.setUser("admin");
		webdavMicroServicesPropertiesPRODModel.setPass("21232f297a57a5a743894a0e4a801fc3");
		prod.setWebdavMicroServicesPropertiesPRODModel(webdavMicroServicesPropertiesPRODModel);
		
		
		URIMicroservicesPropertiesPRODModel uriMicroServicesPropertiesPRODModel = new URIMicroservicesPropertiesPRODModel();
		uriMicroServicesPropertiesPRODModel.setHost("127.0.0.1");
		uriMicroServicesPropertiesPRODModel.setPort("8888");
		uriMicroServicesPropertiesPRODModel.setUser("admin");
		uriMicroServicesPropertiesPRODModel.setPass("21232f297a57a5a743894a0e4a801fc3");
		prod.setUriMicroservicesPropertiesPRODModel(uriMicroServicesPropertiesPRODModel);
		
		evprops.setPRODenvironmentProperties(prod);
		
		//==TEST
		TESTenvironmentProperties test = new TESTenvironmentProperties();
		test.setEnv("TEST");
		MicroservicesPropertiesTESTModel microservicesPropertiesTESTModel = new MicroservicesPropertiesTESTModel();
		microservicesPropertiesTESTModel.setHost("127.0.0.1");
		microservicesPropertiesTESTModel.setPort("9091");
		microservicesPropertiesTESTModel.setUser("admin");
		microservicesPropertiesTESTModel.setPass("21232f297a57a5a743894a0e4a801fc3");
		test.setMicroservicesPropertiesTESTModel(microservicesPropertiesTESTModel);
				
		MongoMicroservicesPropertiesTESTModel mongoMicroservicesPropertiesTESTModel = new MongoMicroservicesPropertiesTESTModel();
		mongoMicroservicesPropertiesTESTModel.setHost("127.0.0.1");
		mongoMicroservicesPropertiesTESTModel.setPort("27019");
		mongoMicroservicesPropertiesTESTModel.setUser("admin");
		mongoMicroservicesPropertiesTESTModel.setPass("21232f297a57a5a743894a0e4a801fc3");
		test.setMongoMicroservicesPropertiesTESTModel(mongoMicroservicesPropertiesTESTModel);
	
		TrainModellingToolPropertiesTESTModel trainModellingToolPropertiesTESTModel = new TrainModellingToolPropertiesTESTModel();
		trainModellingToolPropertiesTESTModel.setHost("127.0.0.1");
		trainModellingToolPropertiesTESTModel.setPort("1880");
		trainModellingToolPropertiesTESTModel.setUser("admin");
		trainModellingToolPropertiesTESTModel.setPass("21232f297a57a5a743894a0e4a801fc3");
		test.setTrainModellingToolPropertiesTESTModel(trainModellingToolPropertiesTESTModel);
		
		
		TrainNodesPropertiesTESTModel trainNodesPropertiesTESTModel = new TrainNodesPropertiesTESTModel();
		trainNodesPropertiesTESTModel.setHost("127.0.0.1");
		trainNodesPropertiesTESTModel.setPort("1880");
		trainNodesPropertiesTESTModel.setUser("admin");
		trainNodesPropertiesTESTModel.setPass("21232f297a57a5a743894a0e4a801fc3");
		test.setTrainNodesPropertiesTESTModel(trainNodesPropertiesTESTModel);
		
		WebdavMicroServicesPropertiesTESTModel webdavMicroServicesPropertiesTESTModel = new WebdavMicroServicesPropertiesTESTModel();
		webdavMicroServicesPropertiesTESTModel.setHost("167.172.175.112");
		webdavMicroServicesPropertiesTESTModel.setPort("9999");
		webdavMicroServicesPropertiesTESTModel.setUser("admin");
		webdavMicroServicesPropertiesTESTModel.setPass("admin");
		test.setWebdavMicroServicesPropertiesTESTModel(webdavMicroServicesPropertiesTESTModel);
		
		WebdavMetadataPropertiesTESTModel webdavMetadataPropertiesTESTModel = new WebdavMetadataPropertiesTESTModel();
		webdavMetadataPropertiesTESTModel.setHost("167.172.175.112");
		webdavMetadataPropertiesTESTModel.setPort("9999");
		webdavMetadataPropertiesTESTModel.setUser("admin");
		webdavMetadataPropertiesTESTModel.setPass("admin");
		test.setWebdavMetadataPropertiesTESTModel(webdavMetadataPropertiesTESTModel);
		
		WebdavDocumentationPropertiesTESTModel webdavDocumentationPropertiesTESTModel = new WebdavDocumentationPropertiesTESTModel();
		webdavDocumentationPropertiesTESTModel.setHost("167.172.175.112");
		webdavDocumentationPropertiesTESTModel.setPort("9997");
		webdavDocumentationPropertiesTESTModel.setUser("admin");
		webdavDocumentationPropertiesTESTModel.setPass("admin");
		test.setWebdavDocumentationPropertiesTESTModel(webdavDocumentationPropertiesTESTModel);
		
		WebdavPagePropertiesTESTModel webdavPagePropertiesTESTModel = new WebdavPagePropertiesTESTModel();
		webdavPagePropertiesTESTModel.setHost("167.172.175.112");
		webdavPagePropertiesTESTModel.setPort("9998");
		webdavPagePropertiesTESTModel.setUser("admin");
		webdavPagePropertiesTESTModel.setPass("admin");
		test.setWebdavPagePropertiesTESTModel(webdavPagePropertiesTESTModel);
		
		
		URIMicroservicesPropertiesTESTModel uriPagePropertiesTESTModel = new URIMicroservicesPropertiesTESTModel();
		uriPagePropertiesTESTModel.setHost("127.0.0.1");
		uriPagePropertiesTESTModel.setPort("8888");
		uriPagePropertiesTESTModel.setUser("admin");
		uriPagePropertiesTESTModel.setPass("admin");
		test.setUriMicroservicesPropertiesTESTModel(uriPagePropertiesTESTModel);
		
		
		evprops.setTESTenvironmentProperties(test);

		return evprops;
		
	}

	
}
